import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { Container, Row, Col, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Card, CardBody, CardTitle, CardText } from 'reactstrap';
import { show_alerta } from '../function';

const ShowMedicamentos = () => {
    const url = 'https://farmacia-api-f52t.onrender.com/api/medicamentos';
    const [medicamentos, setMedicamentos] = useState([]);
    const [id, setId] = useState('');
    const [nombre, setNombre] = useState('');
    const [descripcion, setDescripcion] = useState('');
    const [precio, setPrecio] = useState('');
    const [laboratorio, setLaboratorio] = useState('');
    const [fechaCaducidad, setFechaCaducidad] = useState('');
    const [imagen, setImagen] = useState([]);
    const [operation, setOperaciones] = useState(1);
    const [title, setTitle] = useState('');
    const [op, setOp] = useState('');

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    const openModal = () => {
        setOp(1);
        setNombre('');
        setDescripcion('');
        setPrecio('');
        setLaboratorio('');
        setFechaCaducidad('');
        setImagen(null);
        toggle();
    };

    const validar = (e) => {
        e.preventDefault();
        if(nombre.trim() === ''){
            show_alerta('ESCRIBA EL NOMBRE DEL MEDICAMENTO', 'warning');
        } else if(descripcion.trim() === ''){
            show_alerta('ESCRIBA LA DESCRIPCIÓN DEL MEDICAMENTO', 'warning');
        } else if(precio.trim() === '' || isNaN(precio)){
            show_alerta('ESCRIBA EL PRECIO DEL MEDICAMENTO', 'warning');
        } else if(laboratorio.trim() === ''){
            show_alerta('ESCRIBA EL LABORATORIO DEL MEDICAMENTO', 'warning');
        } else if(fechaCaducidad.trim() === ''){
            show_alerta('ESCRIBA LA FECHA DE CADUCIDAD DEL MEDICAMENTO', 'warning');
        } else {
            let form = new FormData();
            form.append('nombre', nombre);
            form.append('descripcion', descripcion);
            form.append('precio', precio);
            form.append('laboratorio', laboratorio);
            form.append('fecha_caducidad', fechaCaducidad);
            form.append('imagen', imagen);

            let u;
            let metodo;
            if(op == 1){
                u = url;
                metodo = 'POST';
            } else if(op == 2){
                u = `${url}/${id}`;
                metodo = 'PUT';
            }

            enviarSolicitud(u, metodo, form);
        }
    };

    const enviarSolicitud = async (ruta, metodo, parametros) => {
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        };
        await axios({ method: metodo, url: ruta, data: parametros, config })
            .then(function(respuesta){
                if(respuesta.status == 200){
                    show_alerta(respuesta.data.message, 'success');
                    getMedicamentos();
                }
                document.getElementById('btnCerrar').click();
            })
            .catch(function(error){
                console.log(error);
            });
    };

    const deleteMedicamento = (id, nombre) => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: `¿Seguro de eliminar ${nombre}?`,
            icon: 'question',
            text: 'No se podrá dar marcha atrás',
            showCancelButton: true,
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if(result.isConfirmed){
                setId(id);
                enviarSolicitud(`${url}/${id}`, 'DELETE', {});
            } else {
                show_alerta('El Medicamento NO fue eliminado', 'info');
            }
        });
    };

    const getDetalle = (id, n, d, p, l, f, i) => {
        setId(id);
        setOp(2);
        setNombre(n);
        setDescripcion(d);
        setPrecio(p);
        setLaboratorio(l);
        setFechaCaducidad(f);
        setImagen(i);
        toggle();
    };

    useEffect(() => {
        getMedicamentos();
    }, []);

    const getMedicamentos = async () => {
        const respuesta = await axios.get(url);
        setMedicamentos(respuesta.data.data);
    };

    return (
        <Container className='mt-4'>
            <Button onClick={() => openModal()} className="col-md-5 offset-4 btn btn-dark">
                <i className='fa-solid fa-circle-plus mt-2'></i> AGREGAR
            </Button>
            <Row className='mt-3'>
                <Col>
                    <Card className='border border-warning shadow'>
                        <CardTitle className='bg-warning'>
                            <CardText className='h3 text-center'>MEDICAMENTOS</CardText>
                        </CardTitle>
                        <CardBody>
                            <Table responsive bordered>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>NOMBRE</th>
                                        <th>DESCRIPCIÓN</th>
                                        <th>PRECIO</th>
                                        <th>LABORATORIO</th>
                                        <th>FECHA DE CADUCIDAD</th>
                                        <th>IMAGEN</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {medicamentos.map((med, i) => (
                                        <tr key={i}>
                                            <td>{i + 1}</td>
                                            <td>{med.nombre}</td>
                                            <td>{med.descripcion}</td>
                                            <td>{med.precio}</td>
                                            <td>{med.laboratorio}</td>
                                            <td>{med.fecha_caducidad}</td>
                                            <td>
                                                <img src={`https://farmacia-api-f52t.onrender.com${med.imagen}`} width={120} />
                                            </td>
                                            <td>
                                                <Button color='success' onClick={() => getDetalle(med._id, med.nombre, med.descripcion, med.precio, med.laboratorio, med.fecha_caducidad, med.imagen)}>
                                                    <i className="fa-solid fa-edit"></i>
                                                </Button>
                                                &nbsp;
                                                <Button color='danger' onClick={() => deleteMedicamento(med._id, med.nombre)}>
                                                    <i className="fa-solid fa-trash"></i>
                                                </Button>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle} className='text-uppercase'>
                    <p>MEDICAMENTO</p>
                </ModalHeader>
                <ModalBody>
                    <input type="text" className='form-control mb-3' placeholder='NOMBRE' value={nombre} onChange={(e) => setNombre(e.target.value)} />
                    <input type="text" className='form-control mb-3' placeholder='DESCRIPCIÓN' value={descripcion} onChange={(e) => setDescripcion(e.target.value)} />
                    <input type="text" className='form-control mb-3' placeholder='PRECIO' value={precio} onChange={(e) => setPrecio(e.target.value)} />
                    <input type="text" className='form-control mb-3' placeholder='LABORATORIO' value={laboratorio} onChange={(e) => setLaboratorio(e.target.value)} />
                    <input type="date" className='form-control mb-3' placeholder='FECHA DE CADUCIDAD' value={fechaCaducidad} onChange={(e) => setFechaCaducidad(e.target.value)} />
                    <input type="file" className='form-control mb-3' onChange={(e) => setImagen(e.target.files[0])} />
                    <Button color='dark' onClick={validar}><i className="fa-regular fa-floppy-disk"></i></Button>
                </ModalBody>
                <ModalFooter>
                    <Button type='button' className='btn btn-secondary' id='btnCerrar' data-bs-dismiss='modal' onClick={toggle}>
                        CERRAR
                    </Button>
                </ModalFooter>
            </Modal>
        </Container>
    );
};

export default ShowMedicamentos;
