import {Routes, Route, BrowserRouter } from "react-router-dom";
import ShowMedicamentos from './components/ShowMedicamentos';
import ProtectedRoutes from "./components/ProtectedRoutes";
import Login from "./Login";
import Register from "./Register";



function App() {

  return (
  <BrowserRouter>
  <Routes>
    <Route path='/login' element={<Login />}></Route>  
    <Route path='/register' element={<Register />}></Route>
    <Route element={<ProtectedRoutes/>}>
     <Route path='/' element={<ShowMedicamentos></ShowMedicamentos>}></Route>
    </Route> 
  </Routes>
  </BrowserRouter>
  )
}

export default App
