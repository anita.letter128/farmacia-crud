import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { sendRequest } from './function';
import DivInput from './components/DivInput';
import storage from './Storage/storage';

const Register = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const go = useNavigate();
    

    const login = async(e) => {
        e.preventDefault();
        await csrf();
        const form = { name: name,email: email,password: password };
        const res = await sendRequest('POST', form, '/api/usuarios', '', false);
        if (res.status === true) {
            go('/login');
        }
    }

    return (
        <div className='container-fluid'>
            <div className='row mt-5'>
                <div className='col-md-4 offset-md-4'>
                    <div className='card border border-primary'>
                        <div className='card-header bg-primary border border-primary text-white'>
                            REGISTER
                        </div>
                        <div className='card-body'>
                            <form onSubmit={Register}>
                                <DivInput
                                    placeholder="Name"
                                    type="text"
                                    icon='fa-user'
                                    value={name}
                                    className='form-control'
                                    required='required'
                                    handleChange={(e) => setName(e.target.value)}
                                />
                                <DivInput
                                    placeholder="Email"
                                    type="email"
                                    icon='fa-at'
                                    value={email}
                                    className='form-control'
                                    required='required'
                                    handleChange={(e) => setEmail(e.target.value)}
                                />
                                <DivInput
                                    placeholder="Password"
                                    type="password"
                                    icon='fa-key'
                                    value={password}
                                    className='form-control'
                                    required='required'
                                    handleChange={(e) => setPassword(e.target.value)}
                                />
                                <div className='d-grid col-10 mx-auto'>
                                    <button className='btn btn-dark'>
                                        <i className='fa-solid fa-door-open'></i> Register
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Register;
